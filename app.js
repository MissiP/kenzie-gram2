const express = require("express")
const multer = require("multer")
const fs = require("fs")
const pug = require("pug")
const path = ("./public/")
const readPath = ('./public/uploads')
const upload = multer({ dest: readPath })
const port = process.env.PORT || 3400

const app = express()

app.use(express.static(path));

app.post("/upload", upload.single("image"), (req, res) => {
    console.log("Uploaded: " + req.file.filename);
    res.render ("indexback", {img_path_var: req.file.filename})
})
app.get("/", function (req, res) {
//    let feed = ``;
   fs.readdir(readPath, function (err, items) {
       console.log(items);
       res.render("index", { pics: items })
    })
})
app.set("view engine", "pug");

app.listen(port, () => console.log(`Server started on port ${port}`));